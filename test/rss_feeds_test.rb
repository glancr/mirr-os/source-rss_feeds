# frozen_string_literal: true

require 'test_helper'

class RssFeeds::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, RssFeeds
  end
end
