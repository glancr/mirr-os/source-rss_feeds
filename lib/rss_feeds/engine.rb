# frozen_string_literal: true

module RssFeeds
  class Engine < ::Rails::Engine
    isolate_namespace RssFeeds
    config.generators.api_only = true
  end
end
